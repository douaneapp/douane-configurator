"""
Uninstall the Douane configurator from your system.

Usage:

   python3 uninstall.py
"""

import os
import shutil
import site

python_dist_package_path = site.getsitepackages()[0]


print('Removing egg info')
egg_info_file_path = os.path.join(python_dist_package_path,
                                  'douane_configurator-0.1.9.egg-info')
print(f'Removing {egg_info_file_path}')
try:
    os.remove(egg_info_file_path)
except FileNotFoundError:
    pass

print('Removing data')

kcm_douane_desktop_file_path = '/usr/share/kservices5/kcm_douane.desktop'
print(f'Removing {kcm_douane_desktop_file_path}')
try:
    os.remove(kcm_douane_desktop_file_path)
except FileNotFoundError:
    pass

douane_desktop_file_path = '/usr/share/applications/douane-configurator.desktop'
print(f'Removing {douane_desktop_file_path}')
try:
    os.remove(douane_desktop_file_path)
except FileNotFoundError:
    pass


print('Removing scripts')

binary_path = '/usr/local/bin/douane-configurator'
print(f'Removing {binary_path}')
try:
    os.remove(binary_path)
except FileNotFoundError:
    pass


print('Removing lib')

lib_folder_path = os.path.join(python_dist_package_path, 'douane')
print(f'Removing {lib_folder_path}')
shutil.rmtree(lib_folder_path, ignore_errors=True)
